#ifndef PRODUCER_H__
#define PRODUCER_H__

#include <ros/ros.h>

#include <jde_tests/bag_reader.h>
#include "jde_tests/tsc_time.h"

template<class T> class Producer;

#include "jde_tests/DDSProducer.h"
#include "jde_tests/DDSProducerCrypto.h"
#include "jde_tests/DDSProducerAuth.h"
#include "jde_tests/ROSProducer.h"
#include "jde_tests/IceProducer.h"
#include "jde_tests/IceProducerSec.h"


template<class T>
class Producer
{
public:
	Producer()
	{
		ros::NodeHandle nh("~");

		std::string comms("ROS");
		std::string src_topic(""), dst_topic("/default");
		nh.param("comms", comms, comms);
		nh.param("src_topic", src_topic, src_topic);
		nh.param("dst_topic", dst_topic, dst_topic);

		if(comms == "DDS")
		{
			if(std::is_same<T, sensor_msgs::Image>::value)
			producer = new DDSProducer<T, Image, ImagePubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, sensor_msgs::LaserScan>::value)
			producer = new DDSProducer<T, LaserScan, LaserScanPubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, sensor_msgs::PointCloud2>::value)
			producer = new DDSProducer<T, PointCloud, PointCloudPubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, roseus::StringStamped>::value)
			producer = new DDSProducer<T, Text, TextPubSubType>(src_topic, dst_topic);
			else
			{
				ROS_ERROR("Using generic Data type ");
				producer = new DDSProducer<T, Data, DataPubSubType>(src_topic, dst_topic);
			}

		}else if(comms == "DDSCrypt")
		{
			if(std::is_same<T, sensor_msgs::Image>::value)
			producer = new DDSProducerCrypto<T, Image, ImagePubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, sensor_msgs::LaserScan>::value)
			producer = new DDSProducerCrypto<T, LaserScan, LaserScanPubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, sensor_msgs::PointCloud2>::value)
			producer = new DDSProducerCrypto<T, PointCloud, PointCloudPubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, roseus::StringStamped>::value)
			producer = new DDSProducerCrypto<T, Text, TextPubSubType>(src_topic, dst_topic);
			else
			{
				ROS_ERROR("Using generic Data type ");
				producer = new DDSProducer<T, Data, DataPubSubType>(src_topic, dst_topic);
			}

		}else if(comms == "DDSAuth")
		{
			if(std::is_same<T, sensor_msgs::Image>::value)
			producer = new DDSProducerAuth<T, Image, ImagePubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, sensor_msgs::LaserScan>::value)
			producer = new DDSProducerAuth<T, LaserScan, LaserScanPubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, sensor_msgs::PointCloud2>::value)
			producer = new DDSProducerAuth<T, PointCloud, PointCloudPubSubType>(src_topic, dst_topic);
			else if (std::is_same<T, roseus::StringStamped>::value)
			producer = new DDSProducerAuth<T, Text, TextPubSubType>(src_topic, dst_topic);
			else
			{
				ROS_ERROR("Using generic Data type ");
				producer = new DDSProducer<T, Data, DataPubSubType>(src_topic, dst_topic);
			}

		}
		else if(comms == "Ice")
			producer = new IceProducer<T>(src_topic, dst_topic);
		else if(comms == "IceSec")
			producer = new IceProducerSec<T>(src_topic, dst_topic);
		else
			producer = new ROSProducer<T>(src_topic, dst_topic);
	}

	Producer(bool f){};

	~Producer()
	{
		//delete producer;
	}

private:
	Producer<T> *producer;

};

#endif
