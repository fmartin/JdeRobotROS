#ifndef ICE_PRODUCERSEC_H__
#define ICE_PRODUCERSEC_H__

#include <ros/package.h>
#include <thread>

#include <Ice/Ice.h>
#include <IceStorm/IceStorm.h>
#include "jde_comms/ICEJdeModule.h"

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include "jde_tests/TSC_Chrono_producer.h"

template<class T>
class IceProducerSec: public Producer<T>, public TSCChronoProducer
{
public:
	IceProducerSec(std::string src_topic, std::string dst_topic):
	Producer<T>(false),
	src_topic_(src_topic)
	{
		data_sub_ = nh_.subscribe(src_topic, 1, &IceProducerSec::dataCb, this);

		try
		{
			Ice::InitializationData id;
			id.properties = Ice::createProperties();
			std::string path = ros::package::getPath("jde_tests")+"/iceconfigsec/"+"configsec.pub";
			ROS_INFO("Loading config from [%s]", path.c_str());
			id.properties->load(path.c_str());
			ic_ = Ice::initialize(id);

			topic_mgr_ = IceStorm::TopicManagerPrx::checkedCast(
				ic_->propertyToProxy("TopicManager.Proxy"));

			}catch (const Ice::Exception& e)
			{
				std::cerr << e << std::endl;
			}

			if(!topic_mgr_)
			{
				std::cerr <<": invalid proxy" << std::endl;
				return;
			}

		try
			{
				topic_ = topic_mgr_->retrieve(dst_topic);
			}
			catch(const IceStorm::NoSuchTopic&)
			{
				try
				{
					topic_ = topic_mgr_->create(dst_topic);
				}
				catch(const IceStorm::TopicExists&)
				{
					std::cerr << ": temporary failure. try again." << std::endl;
					return;
				}
			}


			Ice::ObjectPrx publisher = topic_->getPublisher();
			//publisher = publisher->ice_datagram();
			publisher = publisher->ice_oneway();

			ice_pub_ = JdeModule::DataConsumerPrx::uncheckedCast(publisher);
			worker_ = std::thread(&IceProducerSec::doWork, this);

	}

		void doWork()
		{
			ros::NodeHandle nh("~");
			std::string bag_file;
			nh.param("bag_file", bag_file, bag_file);

			BagReader<T> reader(bag_file, src_topic_);
			while(reader.has_next())
			{
				typename T::ConstPtr data = reader.getNext();

				if(data != nullptr)
				{
					T msg = *data;

					msg.header.stamp = ros::Time::now();
					dataCb(msg);
				}
			}

			finish();
		}

		void dataCb(const T& msg)
		{
			TscInfo tsci;
			unsigned cycles_low, cycles_high;

			asm volatile ("CPUID\n\t"
			"RDTSC\n\t"
			"mov %%edx, %0\n\t"
			"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
			(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
			tsci.tsc = ( ((uint64_t)cycles_high << 32) | cycles_low );

			uint32_t serial_size = ros::serialization::serializationLength(msg);

			std::vector<uint8_t> buffer(serial_size);

			ros::serialization::OStream stream(buffer.data(), serial_size);
			ros::serialization::serialize(stream, msg);

			JdeModule::JdeDataPtr imd(new JdeModule::JdeData());

			std::vector<uint8_t>::iterator it;
			for(it=buffer.begin(); it!=buffer.end(); ++it)
			imd->data.push_back(*it);

			ros::Time start2 = ros::Time::now();

			ice_pub_->pushData(imd);

			tsci.seq = msg.header.seq;
			takeTSC(tsci);

		}

	private:
		ros::NodeHandle nh_;
		ros::Subscriber data_sub_;

		Ice::CommunicatorPtr ic_;
		IceStorm::TopicManagerPrx topic_mgr_;
		IceStorm::TopicPrx topic_;
		JdeModule::DataConsumerPrx ice_pub_;

		std::thread worker_;
		std::string src_topic_;

};

#endif
