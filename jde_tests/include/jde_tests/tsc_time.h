#ifndef TSC_TIME_H
#define TSC_TIME_H

#include <ros/ros.h>
#include <ros/package.h>

#include <iostream>
#include <fstream>
#include <inttypes.h>
#include "jde_tests/Finish.h"

struct TscInfo
{
	uint32_t seq;
	uint64_t tsc;
};

inline uint64_t getTSC()
{
	unsigned cycles_low, cycles_high;
	asm volatile ("CPUID\n\t"
	"RDTSC\n\t"
	"mov %%edx, %0\n\t"
	"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
	(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
	return ( ((uint64_t)cycles_high << 32) | cycles_low );
}

void processData(const std::vector<jde_tests::Sample>& pub_data, const std::vector<TscInfo> con_data)
{
	ROS_INFO("Processing Non ROS Data [%lu, %lu]", pub_data.size(), con_data.size());

	ros::NodeHandle nh("~");
	std::ofstream file;
	std::string comms;
	std::string data;
	std::string filename;

	nh.param("comms", comms, std::string("NOCOMMS"));
	nh.param("data", data, std::string("NODATA"));

	filename = ros::package::getPath("jde_tests")+"/results/tsc_"+data+"_"+comms+".out";

	ROS_INFO("saving in %s", filename.c_str());
	file.open (filename);

	std::vector<jde_tests::Sample>::const_iterator it1 = pub_data.begin();
	std::vector<TscInfo>::const_iterator it2 = con_data.begin();


	while( (it1!= pub_data.end()) && (it2!=con_data.end()))
	{

		while((it1!=pub_data.end()) && (it1->seq < it2->seq))
		{
			ROS_INFO("[%d] lost", it1->seq);
			file<<"["<<it1->seq<<"] lost\n";
			++it1;
		}

		while((it2!=con_data.end()) && (it2->seq < it1->seq))
		{
			ROS_INFO("[%d] lost", it2->seq);
			file<<"["<<it2->seq<<"] lost\n";
			++it2;
		}

		uint32_t seq1 = it1->seq;
		uint32_t seq2 = it2->seq;


		uint64_t diff = ((it2->tsc-it1->tsc)*625)/1000000;
		file<<"["<<seq1<<"]"<<diff<<" us\n";

		ROS_INFO("Publisher sample [%d] %" PRIu64 "", seq1, it1->tsc);
		ROS_INFO("Consumer  sample [%d] %" PRIu64 "", seq2, it2->tsc);
		ROS_INFO("Diff                  %" PRIu64 "", diff);


		++it1;
		++it2;
	}

	file.close();
}

/*void processDataROS(const std::vector<jde_tests::Sample>& pub_data, const std::vector<TscInfo> con_data)
{

	ROS_INFO("Processing ROS Data");

	ros::NodeHandle nh("~");
	std::ofstream file;
	std::string comms;
	std::string data;
	std::string filename;

	nh.param("comms", comms, std::string("NOCOMMS"));
	nh.param("data", data, std::string("NODATA"));

	filename = ros::package::getPath("jde_tests")+"/results/tsc_"+data+"_"+comms+".out";

	ROS_INFO("saving in %s", filename.c_str());
	file.open (filename);


	std::vector<jde_tests::Sample>::const_iterator it1 = pub_data.begin();
	std::vector<TscInfo>::const_iterator it2 = con_data.begin();

	while( (it1!= pub_data.end()) && (it2!=con_data.end()))
	{
		std::vector<jde_tests::Sample>::const_iterator it1_next = it1;

		++it1_next;

		while( (it1_next!=pub_data.end()) && (it1_next->tsc < it2->tsc))
		{
			ROS_INFO("[%d] lost", it1->seq);
			file<<"["<<it1->seq<<"] lost \n";
			++it1;
			++it1_next;
		}

		uint32_t seq1 = it1->seq;
		uint32_t seq2 = it2->seq;

		uint64_t diff = ((it2->tsc-it1->tsc)*625)/1000000;
		file<<"["<<seq1<<"]"<<diff<<" us\n";

		ROS_INFO("Publisher sample [%d] %" PRIu64 "", seq1, it1->tsc);
		ROS_INFO("Consumer  sample [%d] %" PRIu64 "", seq2, it2->tsc);
		ROS_INFO("Diff                  %" PRIu64 "", diff);


		++it1;
		++it2;
	}
	file.close();
}*/

#endif
