#ifndef DDS_CONSUMER_H__
#define DDS_CONSUMER_H__

#include <fastrtps/fastrtps_fwd.h>
#include <fastrtps/attributes/SubscriberAttributes.h>
#include <fastrtps/subscriber/SubscriberListener.h>
#include <fastrtps/subscriber/SampleInfo.h>

using namespace eprosima::fastrtps;

#include "jde_comms/DDSDataPubSubTypes.h"
#include "jde_comms/DDSData.h"

#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/attributes/SubscriberAttributes.h>
#include <fastrtps/subscriber/Subscriber.h>
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>
#include <roseus/StringStamped.h>

#include "jde_tests/TSC_Chrono_consumer.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

template<class T1, class T2, class T3>
class DDSConsumer:public SubscriberListener, public Consumer<T1>, public TSCChronoConsumer
{
public:
	DDSConsumer(std::string topic):
	mp_participant(nullptr),
	mp_subscriber(nullptr),
	n_samples(0),
	Consumer<T1>(false)
	{
		init(topic);
	}

	~DDSConsumer()
	{
		Domain::removeParticipant(mp_participant);
	}


	std::string getStringType()
	{
		if(std::is_same<T1, sensor_msgs::Image>::value) return "Image";
		else if (std::is_same<T1, sensor_msgs::LaserScan>::value) return "LaserScan";
		else if (std::is_same<T1, sensor_msgs::PointCloud2>::value) return "PointCloud";
		else if (std::is_same<T1, roseus::StringStamped>::value) return "Text";
		else{
			ROS_ERROR("getStringType Type not valid");
			return "Data";
		}
	}

	bool init(std::string topic)
	{
		ParticipantAttributes PParam;
		PParam.rtps.defaultSendPort = 10043;
		PParam.rtps.builtin.use_SIMPLE_RTPSParticipantDiscoveryProtocol = true;
		PParam.rtps.builtin.use_SIMPLE_EndpointDiscoveryProtocol = true;
		PParam.rtps.builtin.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
		PParam.rtps.builtin.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
		PParam.rtps.builtin.domainId = 80;
		PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
		PParam.rtps.listenSocketBufferSize = 1024*1024;//921671
		PParam.rtps.setName("Participant_sub");
		mp_participant = Domain::createParticipant(PParam);
		if(mp_participant==nullptr)
		return false;

		//REGISTER THE TYPE

		Domain::registerType(mp_participant,&m_type);
		//CREATE THE SUBSCRIBER
		SubscriberAttributes Rparam;
		Rparam.topic.topicKind = NO_KEY;
		Rparam.topic.topicDataType = getStringType();
		Rparam.topic.topicName = topic;
		Rparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
		Rparam.topic.historyQos.depth = 2;
		Rparam.topic.resourceLimitsQos.max_samples = 50;
		Rparam.topic.resourceLimitsQos.allocated_samples = 20;
		Rparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
		mp_subscriber = Domain::createSubscriber(mp_participant,Rparam,(SubscriberListener*)this);

		if(mp_subscriber == nullptr)
		return false;


		return true;
	}

	void onShutdown(Subscriber* sub)
	{
		ROS_INFO("FINISHING");
	}

	void onNewDataMessage(Subscriber* sub)
	{

		unsigned cycles_low, cycles_high;
		uint64_t tscval;

		if(sub->takeNextData((void*)&m_Data, &m_info))
		{
			if(m_info.sampleKind == ALIVE)
			{
				this->n_samples++;
				// Print your structure data here.
				uint64_t serial_size = m_Data.data().size();

				std::vector<uint8_t> buffer(serial_size);

				memcpy(buffer.data(), m_Data.data().data(), serial_size);

				ros::serialization::IStream stream(buffer.data(), serial_size);

				ros::serialization::deserialize(stream, ros_data_);

				unsigned cycles_low, cycles_high;
				asm volatile ("CPUID\n\t"
				"RDTSC\n\t"
				"mov %%edx, %0\n\t"
				"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
				(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
				uint64_t t =  ( ((uint64_t)cycles_high << 32) | cycles_low );

				TscInfo tsci;
				tsci.seq = ros_data_.header.seq;
				tsci.tsc = t;

				takeTSC(tsci);
			}
		}
	}


private:
	ros::NodeHandle nh_;

	Participant* mp_participant;
	Subscriber* mp_subscriber;
	T3 m_type;

	T2 m_Data;
	SampleInfo_t m_info;
	uint32_t n_samples;
	T1 ros_data_;
};

#endif
