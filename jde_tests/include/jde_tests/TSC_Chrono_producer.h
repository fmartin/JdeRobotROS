#ifndef TSC_CHRONO_PRODUCER
#define TSC_CHRONO_PRODUCER

#include <ros/ros.h>
#include <inttypes.h>

#include "jde_tests/Finish.h"
#include "jde_tests/tsc_time.h"

class TSCChronoProducer
{
public:
	TSCChronoProducer()
	{

	}

	bool finish()
	{
		ros::ServiceClient client = nh_.serviceClient<jde_tests::Finish>("finish");
		jde_tests::Finish srv;

		for(std::vector<TscInfo>::iterator it=tsc_data_.begin(); it!=tsc_data_.end();++it)
		{
				jde_tests::Sample sample;
				sample.seq = it->seq;
				sample.tsc = it->tsc;

				srv.request.samples.push_back(sample);
		}

		srv.request.seq_init = tsc_data_.begin()->seq;
		client.call(srv);
		ROS_INFO("FINISH: sent");
		ros::shutdown();
		ROS_INFO("Shutting down");

	}

	void takeTSC(const TscInfo& tsci)
	{
		tsc_data_.push_back(tsci);
	}

protected:
	ros::NodeHandle nh_;

	std::vector<TscInfo> tsc_data_;
};

#endif
