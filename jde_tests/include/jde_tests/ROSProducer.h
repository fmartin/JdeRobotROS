#ifndef ROS_PRODUCER_H__
#define ROS_PRODUCER_H__

#include <ros/ros.h>
#include <thread>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include "jde_tests/TSC_Chrono_producer.h"

template<class T>
class ROSProducer: public Producer<T>, public TSCChronoProducer
{
public:
	ROSProducer(std::string src_topic, std::string dst_topic):
	Producer<T>(false),
	src_topic_(src_topic),
	seq(0)
	{
		data_sub_ = nh_.subscribe(src_topic, 1, &ROSProducer::dataCb, this);
		data_pub_ = nh_.advertise<T>(dst_topic, 1);

		worker_ = std::thread(&ROSProducer::doWork, this);
	}

	void doWork()
	{
		ros::NodeHandle nh("~");
		std::string bag_file;
		nh.param("bag_file", bag_file, bag_file);

		ROS_INFO("file: [%s]", bag_file.c_str());
		BagReader<T> reader(bag_file, src_topic_);
		while(reader.has_next())
		{
			typename T::ConstPtr data = reader.getNext();

			if(data != nullptr)
			{
				T msg = *data;

				msg.header.stamp = ros::Time::now();
				dataCb(msg);
			}
		}

		finish();
	}

	void dataCb(const T& msg)
	{
		TscInfo tsci;
		unsigned cycles_low, cycles_high;

		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
		(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
		tsci.tsc = ( ((uint64_t)cycles_high << 32) | cycles_low );

		data_pub_.publish(msg);
		tsci.seq = msg.header.seq;
		takeTSC(tsci);

	}

private:
	ros::NodeHandle nh_;
	ros::Subscriber data_sub_;
	ros::Publisher data_pub_;

	std::thread worker_;
	std::string src_topic_;

	uint32_t seq;
};

#endif
