#ifndef TSC_CHRONO_CONSUMER
#define TSC_CHRONO_CONSUMER

#include <ros/ros.h>
#include <inttypes.h>

#include "jde_tests/Finish.h"
#include "jde_tests/tsc_time.h"

class TSCChronoConsumer
{
public:
	TSCChronoConsumer()
	{
		finish_service_ = nh_.advertiseService("finish", &TSCChronoConsumer::finish, this);

	}

	bool finish(jde_tests::Finish::Request  &req,
	         jde_tests::Finish::Response &res)
	{


		ROS_INFO("Requested finalization");
		/*ros::NodeHandle nh("~");
		std::string comms;
		nh.param("comms", comms, std::string("NOCOMMS"));

		if(comms==std::string("ROS"))
			processDataROS(req.samples, tsc_data_);
		else*/
			processData(req.samples, tsc_data_);

		ROS_INFO("Shutting down");
		ros::shutdown();
	  return true;
	}

	void takeTSC(const TscInfo& tsci)
	{
		/*
		TscInfo tsci;

		unsigned cycles_low, cycles_high;
		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
		(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
		tsci.tsc =  ( ((uint64_t)cycles_high << 32) | cycles_low );

	//getTSC();

		tsci.seq = seq;*/
		tsc_data_.push_back(tsci);
	}

protected:
	ros::NodeHandle nh_;

	std::vector<TscInfo> tsc_data_;
	ros::ServiceServer finish_service_;
};

#endif
