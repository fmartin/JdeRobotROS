#ifndef CONSUMER_H__
#define CONSUMER_H__

#include <ros/ros.h>

#include "jde_tests/tsc_time.h"

template<class T> class Consumer;

#include "jde_tests/DDSConsumer.h"
#include "jde_tests/DDSConsumerCrypto.h"
#include "jde_tests/DDSConsumerAuth.h"
#include "jde_tests/ROSConsumer.h"
#include "jde_tests/IceConsumer.h"
#include "jde_tests/IceConsumerSec.h"


template<class T>
class Consumer
{
public:
	Consumer()
	{
		ros::NodeHandle nh("~");

		std::string comms("ROS");
		std::string topic("/default");
		nh.param("comms", comms, comms);
		nh.param("topic", topic, topic);

		if(comms == "DDS")
		{
			if(std::is_same<T, sensor_msgs::Image>::value)
			consumer = new DDSConsumer<T, Image, ImagePubSubType>(topic);
			else if (std::is_same<T, sensor_msgs::LaserScan>::value)
			consumer = new DDSConsumer<T, LaserScan, LaserScanPubSubType>(topic);
			else if (std::is_same<T, sensor_msgs::PointCloud2>::value)
			consumer = new DDSConsumer<T, PointCloud, PointCloudPubSubType>(topic);
			else if (std::is_same<T, roseus::StringStamped>::value)
			consumer = new DDSConsumer<T, Text, TextPubSubType>(topic);
			else
			{
				ROS_ERROR("Using generic Data type ");
				consumer = new DDSConsumer<T, Data, DataPubSubType>(topic);
			}
		}else if(comms == "DDSCrypt")
		{
			if(std::is_same<T, sensor_msgs::Image>::value)
			consumer = new DDSConsumerCrypto<T, Image, ImagePubSubType>(topic);
			else if (std::is_same<T, sensor_msgs::LaserScan>::value)
			consumer = new DDSConsumerCrypto<T, LaserScan, LaserScanPubSubType>(topic);
			else if (std::is_same<T, sensor_msgs::PointCloud2>::value)
			consumer = new DDSConsumerCrypto<T, PointCloud, PointCloudPubSubType>(topic);
			else if (std::is_same<T, roseus::StringStamped>::value)
			consumer = new DDSConsumerCrypto<T, Text, TextPubSubType>(topic);
			else
			{
				ROS_ERROR("Using generic Data type ");
				consumer = new DDSConsumer<T, Data, DataPubSubType>(topic);
			}
		}else if(comms == "DDSAuth")
		{
			if(std::is_same<T, sensor_msgs::Image>::value)
			consumer = new DDSConsumerAuth<T, Image, ImagePubSubType>(topic);
			else if (std::is_same<T, sensor_msgs::LaserScan>::value)
			consumer = new DDSConsumerAuth<T, LaserScan, LaserScanPubSubType>(topic);
			else if (std::is_same<T, sensor_msgs::PointCloud2>::value)
			consumer = new DDSConsumerAuth<T, PointCloud, PointCloudPubSubType>(topic);
			else if (std::is_same<T, roseus::StringStamped>::value)
			consumer = new DDSConsumerAuth<T, Text, TextPubSubType>(topic);
			else
			{
				ROS_ERROR("Using generic Data type ");
				consumer = new DDSConsumer<T, Data, DataPubSubType>(topic);
			}
		}else if(comms == "Ice")
			consumer = new IceConsumer<T>(topic);
		else if(comms == "IceSec")
				consumer = new IceConsumerSec<T>(topic);
		else
			consumer = new ROSConsumer<T>(topic);
	}

	Consumer(bool f){};

	~Consumer()
	{
		//delete consumer;
	}

private:
	Consumer<T> *consumer;


};

#endif
