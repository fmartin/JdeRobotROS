#ifndef BAG_READER_H
#define BAG_READER_H
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include <fastrtps/utils/eClock.h>

template<class T>
class BagReader
{
public:
	BagReader(std::string filename, std::string topic)
	{
		bag_.open(filename, rosbag::bagmode::Read);

		topics_.push_back(topic);

		view_.addQuery(bag_, rosbag::TopicQuery(topics_));
		it_ = view_.begin();

		typename T::ConstPtr data = (*it_).instantiate<T>();
		diff_ = ros::Time::now() - data->header.stamp;
	}

	typename T::ConstPtr getNext()
	{
		if(has_next()) ++it_;

		if(has_next())
		{
			typename T::ConstPtr data = (*it_).instantiate<T>();

			if(data != NULL)
			{
				ros::Time nt = data->header.stamp + diff_;

				while((nt - ros::Time::now()).toNSec() >0);

				return (*it_).instantiate<T>();
			}
		}else
		{
			bag_.close();
			return nullptr;
		}
	}

	bool has_next()
	{
		return it_!=view_.end();
	}

private:

	rosbag::Bag bag_;
	std::vector<std::string> topics_;
	rosbag::View view_;
	ros::Duration diff_;
	rosbag::View::iterator it_;

};

#endif
