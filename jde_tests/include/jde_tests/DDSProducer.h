#ifndef DDS_PRODUCER_H__
#define DDS_PRODUCER_H__

#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/attributes/PublisherAttributes.h>
#include <fastrtps/publisher/Publisher.h>
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>

#include "jde_comms/DDSDataPubSubTypes.h"
#include "jde_comms/DDSData.h"

#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>
#include <fastrtps/rtps/rtps_all.h>
#include <fastrtps/publisher/PublisherListener.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>
#include <roseus/StringStamped.h>

#include "jde_tests/TSC_Chrono_producer.h"
#include <thread>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>


using namespace eprosima::fastrtps;

template<class T1, class T2, class T3>
class DDSProducer:public PublisherListener, public Producer<T1>, public TSCChronoProducer
{
public:
	DDSProducer(std::string src_topic, std::string dst_topic):
	mp_participant(nullptr),
	mp_publisher(nullptr),
	n_matched(0),
	Producer<T1>(false),
	src_topic_(src_topic)
	{
		data_sub_ = nh_.subscribe(src_topic, 1, &DDSProducer::dataCb, this);

		init(dst_topic);
		worker_ = std::thread(&DDSProducer::doWork, this);

	}

	~DDSProducer()
	{
		Domain::removeParticipant(mp_participant);
	}

	void doWork()
	{
		ros::NodeHandle nh("~");
		std::string bag_file;
		nh.param("bag_file", bag_file, bag_file);

		BagReader<T1> reader(bag_file, src_topic_);
		while(reader.has_next())
		{
			typename T1::ConstPtr data = reader.getNext();

			if(data != nullptr)
			{
				T1 msg = *data;

				msg.header.stamp = ros::Time::now();
				dataCb(msg);
			}
		}

		finish();
	}

	std::string getStringType()
	{
		if(std::is_same<T1, sensor_msgs::Image>::value) return "Image";
		else if (std::is_same<T1, sensor_msgs::LaserScan>::value) return "LaserScan";
		else if (std::is_same<T1, sensor_msgs::PointCloud2>::value) return "PointCloud";
		else if (std::is_same<T1, roseus::StringStamped>::value) return "Text";
		else{
			ROS_ERROR("getStringType Type not valid");
			return "Data";
		}
	}

	bool init(std::string dst_topic)
	{
		ParticipantAttributes PParam;
		PParam.rtps.defaultSendPort = 11511;
		PParam.rtps.use_IP6_to_send = true;
		PParam.rtps.builtin.use_SIMPLE_RTPSParticipantDiscoveryProtocol = true;
		PParam.rtps.builtin.use_SIMPLE_EndpointDiscoveryProtocol = true;
		PParam.rtps.builtin.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
		PParam.rtps.builtin.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
		PParam.rtps.builtin.domainId = 80;
		PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
		PParam.rtps.sendSocketBufferSize = 100*1024*1024;//921671
		PParam.rtps.setName("Participant_pub");
		mp_participant = Domain::createParticipant(PParam);
		if(mp_participant==nullptr)
		return false;
		//REGISTER THE TYPE
		Domain::registerType(mp_participant,&m_type);
		//CREATE THE PUBLISHER
		PublisherAttributes Wparam;
		ThroughputControllerDescriptor throughputController{3000000, 10};
		Wparam.throughputController = throughputController;
		Wparam.qos.m_publishMode.kind = ASYNCHRONOUS_PUBLISH_MODE;
		Wparam.topic.topicKind = NO_KEY;
		Wparam.topic.topicDataType = getStringType();
		Wparam.topic.topicName = dst_topic;
		Wparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
		Wparam.topic.historyQos.depth = 2;
		Wparam.topic.resourceLimitsQos.max_samples = 500;
		Wparam.topic.resourceLimitsQos.allocated_samples = 200;
		Wparam.times.heartbeatPeriod.seconds = 0;
		Wparam.times.heartbeatPeriod.fraction = 200*1000*1000;
		Wparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
		Wparam.qos.m_durability.kind = VOLATILE_DURABILITY_QOS;//RELIABLE_RELIABILITY_QOS;
		mp_publisher = Domain::createPublisher(mp_participant,Wparam,(PublisherListener*)this);
		if(mp_publisher == nullptr)
			return false;
	}

	void dataCb(const T1& msg)
	{
		TscInfo tsci;

		unsigned cycles_low, cycles_high;

		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
		(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
		tsci.tsc = ( ((uint64_t)cycles_high << 32) | cycles_low );

		uint64_t serial_size = static_cast<uint64_t>(ros::serialization::serializationLength(msg));

		std::vector<uint8_t> buffer(serial_size);

		ros::serialization::OStream stream(buffer.data(), serial_size);
		ros::serialization::serialize(stream, msg);

		memcpy((void*)m_data.data().data(), (void*)buffer.data(), serial_size);
		m_data.size(serial_size);

		if(n_matched>0)
		{
			mp_publisher->write(&m_data);

			tsci.seq = msg.header.seq;
			takeTSC(tsci);

		}

	}

	void onPublicationMatched(Publisher* pub,MatchingInfo& info)
	{
		if(info.status == MATCHED_MATCHING)
		{
			n_matched++;
			//cout << "Publisher matched"<<endl;
		}
		else
		{
			n_matched--;
			//cout << "Publisher unmatched"<<endl;
		}
	}

private:
	ros::NodeHandle nh_;
	ros::Subscriber data_sub_;

	T2 m_data;
	Participant* mp_participant;
	Publisher* mp_publisher;

	int n_matched;

	T3 m_type;

	std::thread worker_;
	std::string src_topic_;


};

#endif
