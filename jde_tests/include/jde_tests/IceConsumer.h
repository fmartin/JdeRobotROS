#ifndef ICE_CONSUMER_H__
#define ICE_CONSUMER_H__

#include <ros/package.h>

#include <Ice/Ice.h>
#include "jde_comms/ICEJdeModule.h"
#include <IceStorm/IceStorm.h>
#include <IceUtil/IceUtil.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include "jde_tests/TSC_Chrono_consumer.h"



template<class T>
class IceConsumer: public JdeModule::DataConsumer, public Consumer<T>, public TSCChronoConsumer
{
public:
	IceConsumer(std::string topic):
		Consumer<T>(false)
	{
		try
		{
			Ice::InitializationData id;
			id.properties = Ice::createProperties();
			std::string path = ros::package::getPath("jde_tests")+"/iceconfig/"+"config.sub";
			ROS_INFO("Loading config from [%s]", path.c_str());
			id.properties->load(path);
			ic_ = Ice::initialize(id);
			manager_ = IceStorm::TopicManagerPrx::checkedCast(
				ic_->propertyToProxy("TopicManager.Proxy"));

				if(!manager_)
				{
					std::cerr << ": invalid proxy" << std::endl;
					return;
				}

			}catch (const Ice::Exception& e)
			{
				std::cerr << e << std::endl;
			}

			IceStorm::TopicPrx topic_;
			try
			{
				topic_ = manager_->retrieve(topic);
			}
			catch(const IceStorm::NoSuchTopic&)
			{
				try
				{
					topic_ = manager_->create(topic);
				}
				catch(const IceStorm::TopicExists&)
				{
					std::cerr <<": temporary failure. try again." << std::endl;
					return;
				}
			}

			adapter_ = ic_->createObjectAdapter("JdeData.Subscriber");
			Ice::Identity subId;
			subId.name =  IceUtil::generateUUID();


			subscriber_ = (adapter_->add(this, subId))->ice_oneway();
			adapter_->activate();

			IceStorm::QoS qos;
			try
			{
				topic_->subscribeAndGetPublisher(qos, subscriber_);
			}
			catch(const IceStorm::AlreadySubscribed&)
			{
				// If we're manually setting the subscriber id ignore.
				std::cout << "reactivating persistent subscriber" << std::endl;
			}

		}
		~IceConsumer()
		{
			topic_->unsubscribe(subscriber_);
		}

		void pushData(const JdeModule::JdeDataPtr& im, const Ice::Current&)
		{

			uint32_t serial_size = ros::serialization::serializationLength(im->data);
			std::vector<uint8_t> buffer(serial_size);

			memcpy(buffer.data(), im->data.data(), serial_size);

			ros::serialization::IStream stream(buffer.data(), serial_size);
			ros::serialization::deserialize(stream, ros_data_);

			unsigned cycles_low, cycles_high;
			asm volatile ("CPUID\n\t"
			"RDTSC\n\t"
			"mov %%edx, %0\n\t"
			"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
			(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
			uint64_t t =  ( ((uint64_t)cycles_high << 32) | cycles_low );

			TscInfo tsci;
			tsci.seq = ros_data_.header.seq;
			tsci.tsc = t;

			takeTSC(tsci);
		};

	private:
		ros::NodeHandle nh_;

		Ice::CommunicatorPtr ic_;
		Ice::ObjectAdapterPtr adapter_;
		IceStorm::TopicManagerPrx manager_;
		IceStorm::TopicPrx topic_;
		Ice::ObjectPrx subscriber_;


		T ros_data_;

	};

	#endif
