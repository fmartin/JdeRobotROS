#ifndef ROS_CONSUMER_H__
#define ROS_CONSUMER_H__

#include <ros/ros.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include "jde_tests/TSC_Chrono_consumer.h"

#include <ros/transport_hints.h>


template<class T>
class ROSConsumer: public Consumer<T>, public TSCChronoConsumer
{
public:
	ROSConsumer(std::string topic):
		Consumer<T>(false)
	{
		data_sub_ = nh_.subscribe(topic, 1, &ROSConsumer::dataCb, this, ros::TransportHints().udp());
	}

	void dataCb(const T& msg)
	{
		unsigned cycles_low, cycles_high;
		asm volatile ("CPUID\n\t"
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (cycles_high), "=r"
		(cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
		uint64_t t =  ( ((uint64_t)cycles_high << 32) | cycles_low );

		TscInfo tsci;
		tsci.seq = msg.header.seq;
		tsci.tsc = t;

		takeTSC(tsci);

		data_ = msg;

	}

private:
	ros::NodeHandle nh_;
	ros::Subscriber data_sub_;

	T data_;

};

#endif
