#include <ros/ros.h>
#include <ros/package.h>

#include "jde_tests/Consumer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "image_consumer");

	Consumer<sensor_msgs::Image> consumer;

	ros::spin();

	return 0;
}
