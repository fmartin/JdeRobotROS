#include <ros/ros.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_publisher");
  ros::NodeHandle nh;
	int seq = 0;

  cv_bridge::CvImage cv_image;
  cv_image.image = cv::imread("/home/paco/Escritorio/image.jpg",CV_LOAD_IMAGE_COLOR);
  cv_image.encoding = "bgr8";
  sensor_msgs::Image ros_image;
  cv_image.toImageMsg(ros_image);

  ros::Publisher pub = nh.advertise<sensor_msgs::Image>("/camera/rgb/image_raw", 1);
  ros::Rate loop_rate(30);

  while (nh.ok())
  {
		ros_image.header.stamp = ros::Time::now();
		ros_image.header.seq = seq++;;
    pub.publish(ros_image);
    loop_rate.sleep();
  }

	return 0;
}
