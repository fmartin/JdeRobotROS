#include <ros/ros.h>

#include "jde_tests/Producer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "laserscan_producer");

	Producer<sensor_msgs::LaserScan> producer;

	ros::spin();

	return 0;
}
