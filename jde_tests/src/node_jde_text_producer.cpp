#include <ros/ros.h>

#include "jde_tests/Producer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "text_producer");

	Producer<roseus::StringStamped> producer;

	ros::spin();

	return 0;
}
