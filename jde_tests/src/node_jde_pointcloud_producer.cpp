#include <ros/ros.h>

#include "jde_tests/Producer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "pointcloud_producer");

	Producer<sensor_msgs::PointCloud2> producer;

	ros::spin();

	return 0;
}
