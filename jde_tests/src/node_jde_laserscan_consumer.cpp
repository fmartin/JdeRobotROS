#include <ros/ros.h>

#include "jde_tests/Consumer.h"



int main(int argc, char* argv[])
{
	ros::init(argc, argv, "laserscan_consumer");
	ros::NodeHandle n;


	Consumer<sensor_msgs::LaserScan> consumer;

	ros::spin();

	return 0;
}
