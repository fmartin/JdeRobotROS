#include <ros/ros.h>

#include "jde_tests/Producer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "image_producer");

	Producer<sensor_msgs::Image> producer;

	ros::spin();

	return 0;
}
