#include <ros/ros.h>

#include "jde_tests/Consumer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "pointcloud_consumer");

	Consumer<sensor_msgs::PointCloud2> consumer;

	ros::spin();

	return 0;
}
