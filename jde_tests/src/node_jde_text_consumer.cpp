#include <ros/ros.h>
#include <ros/package.h>

#include "jde_tests/Consumer.h"

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "text_consumer");

	Consumer<roseus::StringStamped> consumer;

	ros::spin();

	return 0;
}
