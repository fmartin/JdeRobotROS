#!/bin/bash

if [ x$2 = x ]
then
	echo usage generate name serial >&2
	exit 1
fi

set -x

openssl genrsa -out $1.key 4096

openssl req -new -key $1.key -out $1.csr -days 5000  -subj "/C=DE/ST=NRW/L=Earth/O=Random\Company/OU=IT/CN=$1/\emailAddress=dont@mail.me"

openssl x509 -req -days 5000 -in $1.csr -CA ca.pem -CAkey ca.key -set_serial $2  -out $1cert.pem

openssl verify -verbose -CAfile  <(cat ca.pem) $1cert.pem
