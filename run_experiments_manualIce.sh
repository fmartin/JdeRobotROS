#!/bin/sh

i=IceSec

for j in laser image pointcloud
do
	sleep 1

	echo Executing $i $j

	roslaunch jde_tests jde_test_${j}_consumer.launch  protocol:=$i &>/dev/null &
	roslaunch jde_tests jde_test_${j}_producer.launch  protocol:=$i &>/dev/null

done
