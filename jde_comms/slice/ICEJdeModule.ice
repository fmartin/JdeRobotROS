#ifndef IMAGE_ICE
#define IMAGE_ICE


module JdeModule{

	sequence<byte> ByteSeq;

  class JdeData
  {
    ByteSeq data;
  };


  /**
   * Interface to the image provider.
   */
  interface DataConsumer
  {

    /**
     * Returns the latest data.
     */
    idempotent void pushData(JdeData img);
  };

}; //module

#endif //IMAGE_ICE
