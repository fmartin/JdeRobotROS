file(GLOB idl_files "idl/*.idl")
foreach(idl_file ${idl_files})
		MESSAGE( STATUS "file: " ${idl_file})
    get_filename_component(idl_name ${idl_file} NAME_WE)

    set(cpp_file ${idl_name}.cxx)
    set(header_file ${idl_name}.h)
    set(cpp_file_types ${idl_name}PubSubTypes.cxx)
    set(header_file_types ${idl_name}PubSubTypes.h)

		MESSAGE( STATUS "cpp: " ${cpp_file})
		MESSAGE( STATUS "header: " ${header_file})
		MESSAGE( STATUS "cpp_file_types: " ${cpp_file_types})
		MESSAGE( STATUS "header_file_types: " ${header_file_types})

    add_custom_command(OUTPUT ${cpp_file} ${header_file} ${cpp_file_types} ${header_file_types}
        COMMAND fastrtpsgen -d ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME} -replace ${idl_file}
				COMMAND mv ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}/*.cxx ${PROJECT_BINARY_DIR}
				DEPENDS ${idl_file}
    )

    list(APPEND DDS_SOURCE_FILES ${cpp_file} ${header_file} ${cpp_file_types} ${header_file_types})
		MESSAGE( STATUS "DDS_SOURCE_FILES: " ${DDS_SOURCE_FILES})
endforeach(idl_file)
