file(GLOB ice_files "slice/*.ice")
foreach(ice_file ${ice_files})
		MESSAGE( STATUS "file: " ${ice_file})
    get_filename_component(ice_name ${ice_file} NAME_WE)

    set(cpp_file ${ice_name}.cpp)
    set(header_file -I${PROJECT_SOURCE_DIR}/include/${ice_name}.h)

		#MESSAGE( STATUS "cpp: " ${cpp_file})
		#MESSAGE( STATUS "header: " ${header_file})

    add_custom_command(OUTPUT ${cpp_file} ${header_file}
        COMMAND slice2cpp -I${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME} ${ice_file} --output-dir ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}
				COMMAND mv ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}/${cpp_file} ${PROJECT_BINARY_DIR}
        DEPENDS ${ice_file}
    )

    list(APPEND ICE_SOURCE_FILES ${cpp_file} ${header_file})
		MESSAGE( STATUS "ICE_SOURCE_FILES: " ${ICE_SOURCE_FILES})
endforeach(ice_file)
