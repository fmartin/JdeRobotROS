#ifndef ICE_OBJ__H
#define ICE_OBJ__H

#include <Ice/Ice.h>
#include "ros/ros.h"

template<class IcePrx = Ice::ObjectPrx>
class IceObj
{
public:
	IceObj()
	{
		ros::NodeHandle n("~");

		n.param("name", name_, name_);
		n.param("address", address_, address_);

		try
		{
			ic_ = Ice::initialize();

		}catch (const Ice::Exception& e)
		{
			std::cerr << e << std::endl;
		}
	}

	void registedAsServantIce(Ice::Object *obj)
	{
		adapter_ = ic_->createObjectAdapterWithEndpoints(name_, address_);
		adapter_->add(obj, ic_->stringToIdentity(name_));
		adapter_->activate();
	}

	void initProxy()
	{
		proxy_ = IcePrx::checkedCast(ic_->stringToProxy(name_+":"+address_));
	}

	IcePrx& getProxyIce()
	{
		return proxy_;
	}

	protected:
		Ice::CommunicatorPtr ic_;
		Ice::ObjectAdapterPtr adapter_;
		IcePrx proxy_;
		std::string name_;
		std::string address_;

};

#endif
