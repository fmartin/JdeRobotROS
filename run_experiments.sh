#!/bin/sh

#sudo tc qdisc change dev eth0 root netem delay 25ms 20ms distribution normal

. udp_params.sh
roscore &

for i in DDSAuth #DDSCrypt #DDS #DDSCrypt DDSAuth #Ice IceSec ROS
do
	for j in text
	do
		sleep 1

		echo Executing $i $j

		roslaunch jde_tests jde_test_${j}_consumer.launch  protocol:=$i &>/dev/null &
		roslaunch jde_tests jde_test_${j}_producer.launch  protocol:=$i &>/dev/null

	done
done

#i=Ice
#roscd jde_tests/iceconfig/
#icebox --Ice.Config=config.icebox &
#sleep 3

#for j in laser image pointcloud
#do
#	sleep 1
#
# echo Executing $i $j
#
#	roslaunch jde_tests jde_test_${j}_consumer.launch  protocol:=$i &>/dev/null &
#	roslaunch jde_tests jde_test_${j}_producer.launch  protocol:=$i &>/dev/null

#done

#killall icebox
#sleep 5

#i=IceSec
#roscd jde_tests/iceconfigsec/
#icebox --Ice.Config=configsec.icebox &
#sleep 3
#
#for j in laser image pointcloud
#do
#	sleep 1
#
#	echo Executing $i $j
#
#	roslaunch jde_tests jde_test_${j}_consumer.launch  protocol:=$i &>/dev/null &
#	roslaunch jde_tests jde_test_${j}_producer.launch  protocol:=$i &>/dev/null

#done
#killall icebox
